# preferences.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Flatpak', '1.0')

from gi.repository import GLib, GObject, Gtk, Gio, Handy, Flatpak

from collections import namedtuple
from enum import Enum
from subprocess import Popen, PIPE
from threading import Thread
import os

from .flatpakutils import get_flatpak_installations, make_command

class PropertyType(Enum):
    BOOLEAN = 0

Property = namedtuple('Property', 'id title type')
Page = namedtuple('Page', 'title icon groups')
Group = namedtuple('Group', 'title properties')

properties = [
    Page(_('View'), 'package-x-generic-symbolic',
        [
            Group(_('App list'),
                [
                    Property('runtimes-visible', _('Show runtimes'), PropertyType.BOOLEAN),
                ]
            ),
            Group(_('Installation'),
                [
                    Property('auto-dialog-confirm', _('Do not ask for confirmations'), PropertyType.BOOLEAN)
                ]
            )
        ]
    ),
    Page(_('Updates'), 'software-update-available-symbolic',
        [
            Group(_('Update checking'),
                [
                    Property('check-updates', _('Check updates on login'), PropertyType.BOOLEAN),
                ]
            )
        ]
    )
]

@Gtk.Template(resource_path='/io/gitlab/FlatStore/preferences.ui')
class FlatstorePreferencesWindow(Handy.PreferencesWindow):
    __gtype_name__ = 'FlatstorePreferencesWindow'

    remotes_page = None
    remotes_groups = []

    add_remote_popover = Gtk.Template.Child()
    add_remote_name = Gtk.Template.Child()
    add_remote_url = Gtk.Template.Child()

    add_remote_btn = Gtk.Template.Child()
    add_remote_btn_singal = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.settings = Gio.Settings('io.gitlab.FlatStore')
        for p in properties:
            page = Handy.PreferencesPage.new()
            page.set_title(p.title)
            page.set_icon_name(p.icon)
            # Groups
            for g in p.groups:
                group = Handy.PreferencesGroup.new()
                group.set_title(g.title)
                listbox = Gtk.ListBox.new()
                listbox.set_selection_mode(Gtk.SelectionMode.NONE)
                group.pack_start(listbox, False, False, 0)
                for prop in g.properties:
                    # Add widget
                    switch = Gtk.Switch()
                    switch.set_state(self.settings.get_boolean(prop.id))
                    switch.connect('state-set', self.on_switch_state_set, prop.id)
                    self.add_row_to_lb(listbox, switch, prop.title)
                listbox.show_all()
                page.add(group)
            self.add(page)
            page.show_all()

        self.update_remotes_page()

    def add_row_to_lb(self, listbox, widget, title):
        row = Handy.ActionRow.new()
        row.set_title(title)
        row.set_activatable_widget(widget)
        row.add_action(widget)
        widget.set_valign(Gtk.Align.CENTER)
        listbox.add(row)
        return row

    def on_switch_state_set(self, switch, state, id):
        self.settings.set_boolean(id, state)

    def update_remotes_page(self):
        if self.remotes_page == None:
            self.remotes_page = Handy.PreferencesPage.new()
            self.remotes_page.set_title(_('Remotes'))
            self.remotes_page.set_icon_name('preferences-system-search-symbolic')
            self.add(self.remotes_page)
        else:
            for child in self.remotes_groups:
                child.destroy()
            self.remotes_groups.clear()
        for installation in get_flatpak_installations():
            group = Handy.PreferencesGroup.new()
            name = installation.get_display_name()
            if name == None: name = installation.get_id()
            group.set_title(name)
            listbox = Gtk.ListBox.new()
            listbox.set_selection_mode(Gtk.SelectionMode.NONE)
            group.pack_start(listbox, False, False, 0)
            self.remotes_groups.append(group)
            for remote in installation.list_remotes():
                # Remove button
                btn = Gtk.Button()
                btn.set_label(_('Remove'))
                btn.get_style_context().add_class('destructive-action')
                btn.connect('clicked', self.on_remove_remote_btn_clicked, remote.get_name(), installation)
                btn.set_size_request(90, -1)
                row = self.add_row_to_lb(listbox, btn, remote.get_name())
                desc = remote.get_description()
                if desc != None: row.set_subtitle(desc)
            # Add remote widget
            add_btn = Gtk.Button.new_with_label(_('Add'))
            row = self.add_row_to_lb(listbox, add_btn, _('New remote'))
            add_btn.set_size_request(90, -1)
            add_btn.connect('clicked', self.on_add_remote_row_clicked, installation, row)
            listbox.show_all()
            self.remotes_page.add(group)
        self.remotes_page.show_all()

    def on_remove_remote_btn_clicked(self, btn, remote, installation):
        # installation.remove_remote(remote)
        cmd = make_command('remote-delete {}'.format(remote), installation=installation)
        self.run_command(cmd)

    def on_add_remote_row_clicked(self, btn, installation, row):
        self.add_remote_popover.set_relative_to(row)
        self.add_remote_popover.popup()
        self.add_remote_url.grab_focus()
        if self.add_remote_btn_singal != None:
            self.add_remote_btn.disconnect(self.add_remote_btn_singal)
        self.add_remote_btn_singal = self.add_remote_btn.connect('clicked', self.on_add_remote_btn_clicked, installation)

    def on_add_remote_btn_clicked(self, btn, installation):
        name = self.add_remote_name.get_text()
        url = self.add_remote_url.get_text()
        if url == '' or name == '': return
        cmd = make_command('remote-add {} {}'.format(name, url), installation=installation)
        self.run_command(cmd)
        self.add_remote_popover.popdown()

    @Gtk.Template.Callback()
    def on_add_remote_url_changed(self, editable):
        url = self.add_remote_url.get_text()
        name = os.path.splitext(url[url.rfind("/")+1:])[0]
        self.add_remote_name.set_text(name)

    def run_command(self, cmd):
        def operation():
            p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            output, err = p.communicate()
            rc = p.returncode
            err = err.decode('utf-8')
            GLib.idle_add(show_result, rc, err)
        def show_result(rc, err):
            if rc != 0:
                dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                Gtk.ButtonsType.OK, err)
                dialog.run()
                dialog.destroy()
            self.update_remotes_page()
        t = Thread(target=operation)
        t.start()
