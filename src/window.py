
# window.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Handy', '0.0')
gi.require_version('Flatpak', '1.0')

from gi.repository import GLib, GObject, Gtk, Handy, Gio, Flatpak
from .webstore import FlatstoreWebStore
from .installed import FlatstoreInstalledView
from .updates import FlatstoreUpdatesView
from .preferences import FlatstorePreferencesWindow
from .jobcontainer import FlatstoreJobQueue
from .flatpakutils import get_flatpak_installations, get_remote_ref_from_installed

GObject.type_register(Handy.TitleBar)

PAGE_STORE = 'webstore'
PAGE_INSTALLED = 'installed'
PAGE_UPDATES = 'updates'

@Gtk.Template(resource_path='/io/gitlab/FlatStore/window.ui')
class FlatstoreWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'FlatstoreWindow'

    view_stack = Gtk.Template.Child()
    view_stack_switcher = Gtk.Template.Child()
    view_stack_switcher_bar = Gtk.Template.Child()
    view_btn_back = Gtk.Template.Child()
    header_bar = Gtk.Template.Child()
    preferences_btn = Gtk.Template.Child()

    dbus_autostart_subid = -1

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.settings = Gio.Settings('io.gitlab.FlatStore')
        self.settings.connect('changed', self.on_settings_changed)
        self.set_default_icon_name('io.gitlab.FlatStore')

        # Init job queue
        self.job_queue = FlatstoreJobQueue()

        self.webstore = FlatstoreWebStore()
        self.webstore.connect(
            'notify::can-go-back', self.on_webstore_can_go_back_notify)
        self.webstore.connect('install-app', self.on_webstore_install_app)
        self.webstore.connect('install-app-bundle', self.on_webstore_install_app_bundle)
        self.webstore.connect('remove-app', self.on_webstore_remove_app)
        self.view_stack.add_titled(self.webstore, PAGE_STORE, _('Browse'))
        self.view_stack.set_visible_child(self.webstore)
        self.view_stack.child_set_property(
            self.webstore, 'icon-name', 'web-browser-symbolic')

        self.installed = FlatstoreInstalledView(self.job_queue)
        self.installed.connect('show-store', self.on_installed_show_store)
        self.view_stack.add_titled(
            self.installed, PAGE_INSTALLED, _('Installed'))
        self.view_stack.child_set_property(
            self.installed, 'icon-name', 'package-x-generic-symbolic')

        self.updates = FlatstoreUpdatesView(self.job_queue)
        self.updates.connect('show-store', self.on_installed_show_store)
        self.view_stack.add_titled(
            self.updates, PAGE_UPDATES, _('Updates'))
        self.view_stack.child_set_property(
            self.updates, 'icon-name', 'software-update-available-symbolic')

        #Setup Handy widgets (some props causes Glade crash)
        self.view_stack_switcher.set_stack(self.view_stack)
        self.view_stack_switcher.set_orientation(Gtk.Orientation.HORIZONTAL)

        self.header_bar.add(self.preferences_btn)
        self.header_bar.child_set_property(
            self.preferences_btn, 'pack-type', Gtk.PackType.END)

        if self.settings.get_boolean('first-run'):
            self.settings.set_boolean('check-updates', True)
            self.settings.set_boolean('first-run', False)

    @Gtk.Template.Callback()
    def on_show (self, win):
        self.webstore.refresh()

    @Gtk.Template.Callback()
    def on_view_btn_back_clicked (self, btn):
        self.webstore.go_back()

    @Gtk.Template.Callback()
    def on_view_stack_visible_child_name_notify (self, view_stack, child):
        self.set_go_back_btn_visible()

    def on_webstore_can_go_back_notify(self, webstore, val):
        self.set_go_back_btn_visible()

    def set_go_back_btn_visible(self):
        self.view_btn_back.set_visible(
            self.view_stack.get_visible_child_name() == PAGE_STORE and self.webstore.can_go_back)

    def on_webstore_install_app(self, webstore, ref, app_name, app_icon):
        self.installed.install_app(ref, app_name, app_icon)
        self.view_stack.set_visible_child_full(
            PAGE_INSTALLED, Gtk.StackTransitionType.OVER_LEFT)

    def on_webstore_install_app_bundle(self, webstore, ref):
        self.installed.install_app_bundle(ref)
        self.view_stack.set_visible_child_full(
            PAGE_INSTALLED, Gtk.StackTransitionType.OVER_LEFT)

    def on_webstore_remove_app(self, webstore, ref):
        self.installed.remove_app(ref)
        self.view_stack.set_visible_child_full(
            PAGE_INSTALLED, Gtk.StackTransitionType.OVER_LEFT)

    @Gtk.Template.Callback()
    def on_preferences_btn_clicked(self, btn):
        win = FlatstorePreferencesWindow()
        win.set_transient_for(self)
        win.show_all()

    @Gtk.Template.Callback()
    def on_size_allocate(self, widget, allocation):
        is_narrow = allocation.width < 700
        self.view_stack_switcher_bar.set_reveal(is_narrow)
        self.view_stack_switcher.set_visible(not is_narrow)

        if allocation.width < 700:
            align = Gtk.Align.FILL
        else:
            align = Gtk.Align.CENTER
        self.installed.main_box.set_halign(align)
        self.updates.main_box.set_halign(align)

    def on_settings_changed(self, settings, key):
        if key == 'check-updates':
            proxy = Gio.DBusProxy.new_for_bus_sync(Gio.BusType.SESSION,
                                           Gio.DBusProxyFlags.NONE,
                                           None,
                                           'org.freedesktop.portal.Desktop',
                                           '/org/freedesktop/portal/desktop',
                                           'org.freedesktop.portal.Background',
                                           None)
            result = proxy.RequestBackground('(sa{sv})', str(self.get_id()), {
                'dbus-activatable': GLib.Variant.new_boolean(False),
                'autostart': GLib.Variant.new_boolean(settings.get_boolean(key)),
                'reason': GLib.Variant.new_string(_('Check updates on login')),
                'commandline': GLib.Variant.new_strv(['flatstore', '--auto-update'])
            })
            if result != None:
                request_handle = result
                if self.dbus_autostart_subid != -1:
                    proxy.get_connection().signal_unsubscribe(self.dbus_autostart_subid)
                self.dbus_autostart_subid = proxy.get_connection().signal_subscribe(
                                                        'org.freedesktop.portal.Desktop',
                                                        'org.freedesktop.portal.Request',
                                                        'Response',
                                                        request_handle,
                                                        None,
                                                        Gio.DBusSignalFlags.NO_MATCH_RULE,
                                                        self.on_autostart_set_result)

    def on_autostart_set_result(self, connection, sender, path, interface, signal, params):
        if not isinstance(params, GLib.Variant):
            return
        response_code, results = params.unpack()
        if (results['autostart'] == False and self.settings.get_boolean('check-updates')):
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                                       Gtk.ButtonsType.OK,
                                       _('Cannot enable auto update checking'))
            dialog.format_secondary_text(
                _('Please check app permissions.'))
            dialog.run()
            dialog.destroy()

    def on_installed_show_store(self, obj, ref, installation):
        self.open_app(get_remote_ref_from_installed(ref, installation), installation)

    def open_app(self, ref, installation):
        handler_id = 0
        def show_app(*args):
            self.webstore.show_app(ref, installation = installation)
            self.webstore.disconnect(handler_id)
        if self.webstore.loading_appstream:
            handler_id = self.webstore.connect('notify::loading-appstream', show_app)
        else:
            show_app()

        self.view_stack.set_visible_child_full(
            PAGE_STORE, Gtk.StackTransitionType.OVER_RIGHT)

    def open_local_file(self, path):
        ref_file = Gio.File.new_for_path(path)
        try:
            file_type = ref_file.query_info(Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                                            Gio.FileQueryInfoFlags.NONE).get_content_type()
        except GLib.Error as e:
            self.open_local_file_error(e.message, path)
            return

        if file_type == 'application/vnd.flatpak':
            bundle = Flatpak.BundleRef.new(ref_file)
            self.open_app(bundle, None)
        elif file_type == 'application/vnd.flatpak.ref':
            try:
                f = open(path, 'rb')
                data = f.read()
                data = GLib.Bytes.new(data)
                installation = get_flatpak_installations()[0]
                try:
                    ref = installation.install_ref_file(data)
                    ref = get_remote_ref_from_installed(ref, installation)
                except GLib.Error as e:
                    if e.code == Flatpak.Error.ALREADY_INSTALLED:
                        kf = GLib.KeyFile.new()
                        kf.load_from_bytes(data, GLib.KeyFileFlags.NONE)
                        ref = installation.get_installed_ref(
                            Flatpak.RefKind.RUNTIME if kf.get_boolean('Flatpak Ref', 'IsRuntime') else Flatpak.RefKind.APP,
                            kf.get_string('Flatpak Ref', 'Name'),
                            None,
                            kf.get_string('Flatpak Ref', 'Branch')
                        )
                        ref = get_remote_ref_from_installed(ref, installation)
                self.open_app(ref, installation)
                f.close()
            except IOError as err:
                self.open_local_file_error(str(e), path)
        else:
            self.open_local_file_error(_('Invalid file type.'), path)

    def open_local_file_error(self, err, path):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                                       Gtk.ButtonsType.OK,
                                       _('Cannot open file {}.').format(path))
        dialog.format_secondary_text(err)
        dialog.run()
        dialog.destroy()
