# permissionsdialog.py
#
# Copyright 2020 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import gi
gi.require_version('Flatpak', '1.0')

from gi.repository import GLib, GObject, Gtk, Gio, Handy
from gi.repository import Flatpak

@Gtk.Template(resource_path='/io/gitlab/FlatStore/permissionsdialog.ui')
class FlatstorePermissionsDialog(Gtk.Dialog):
    __gtype_name__ = 'FlatstorePermissionsDialog'

    header_bar = Gtk.Template.Child()
    view_stack = Gtk.Template.Child()
    view_stack_switcher = Gtk.Template.Child()

    accept_btn = Gtk.Template.Child()
    cancel_btn = Gtk.Template.Child()

    details_label = Gtk.Template.Child()

    box_fs_home = Gtk.Template.Child()
    box_fs = Gtk.Template.Child()
    details_fs = Gtk.Template.Child()
    box_devices = Gtk.Template.Child()
    details_devices = Gtk.Template.Child()
    box_shared_network = Gtk.Template.Child()
    box_dbus_flatpak = Gtk.Template.Child()
    box_dbus_location = Gtk.Template.Child()
    box_dbus_notifications = Gtk.Template.Child()

    def __init__(self, remote_ref: Flatpak.Ref, show_buttons = False, **kwargs):
        super().__init__(**kwargs)
        metadata = remote_ref.get_metadata()
        kf = GLib.KeyFile.new()
        kf.load_from_bytes(metadata, GLib.KeyFileFlags.NONE)
        try: shared = kf.get_string_list('Context', 'shared')
        except: shared = []
        try: sockets = kf.get_string_list('Context', 'sockets')
        except: sockets = []
        try: devices = kf.get_string_list('Context', 'devices')
        except: devices = []
        try: filesystems = kf.get_string_list('Context', 'filesystems')
        except: filesystems = []
        try: dbus = kf.get_keys('Session Bus Policy')[0]
        except: dbus = []

        # Filesystem
        self.box_fs_home.set_visible('home' in filesystems)
        if 'home' in filesystems: filesystems.remove('home')
        if 'host' in filesystems: filesystems = [_('Full access')]
        elif 'host:ro' in filesystems: filesystems = ['{} ({})'.format(_('Full access'), _('read only'))]
        else: filesystems = [i.replace(':ro', ' ({})'.format(_('read only'))) for i in filesystems]
        self.box_fs.set_visible(len(filesystems) > 0)
        self.details_fs.set_text(', '.join(filesystems))
        # Devices
        if 'dri' in devices: devices.remove('dri') # No need to stress the user with this
        self.box_devices.set_visible(len(devices) > 0)
        self.details_devices.set_label(', '.join(devices))

        self.box_shared_network.set_visible('network' in shared)
        self.box_dbus_flatpak.set_visible('org.freedesktop.Flatpak' in dbus)
        self.box_dbus_location.set_visible('org.freedesktop.GeoClue2' in dbus)
        self.box_dbus_notifications.set_visible('org.freedesktop.Notifications' in dbus)

        categories = (_('Shared'), _('Sockets'), _('Devices'), _('File systems'), _('DBus'))
        details = '<b>{}:</b>\n{}\n\n<b>{}:</b>\n{}\n\n<b>{}:</b>\n{}\n\n<b>{}:</b>\n{}\n\n'.format(
            categories[0], '\n'.join(shared),
            categories[1], '\n'.join(sockets),
            categories[2], '\n'.join(devices),
            categories[3], '\n'.join(filesystems)
        )
        details += '<b>{}:</b>\n'.format(categories[4])
        details += '\n'.join(['{} ({})'.format(i, kf.get_string('Session Bus Policy', i)) for i in dbus])
        self.details_label.set_markup(details)

        self.accept_btn.set_visible(show_buttons)
        self.cancel_btn.set_visible(show_buttons)

        # Handy stack switcher
        self.set_titlebar(self.header_bar)
        self.view_stack_switcher.set_stack(self.view_stack)
        self.view_stack_switcher.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.header_bar.set_custom_title(self.view_stack_switcher)
