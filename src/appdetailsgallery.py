# appdetailsgallery.py
#
# Copyright 2020 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib, GObject, Gtk, Gdk, Gio
from gi.repository import GdkPixbuf

@Gtk.Template(resource_path='/io/gitlab/FlatStore/appdetailsgallery.ui')
class FlatstoreDetailsGallery(Gtk.Box):
    __gtype_name__ = 'FlatstoreDetailsGallery'
    btn_prev = Gtk.Template.Child()
    btn_next = Gtk.Template.Child()
    stack = Gtk.Template.Child()

    __gsignals__ = {
        # This is emitted when image is switched or currnet image is loaded
        'image-switched': (GObject.SIGNAL_RUN_FIRST, None, ())
    }

    def __init__(self, create_fullsize_dialog = True, **kwargs):
        super().__init__(kwargs)
        self.images = []
        self.create_fullsize_dialog = create_fullsize_dialog
        if create_fullsize_dialog:
            self.dialog = FlatstoreDetailsGalleryDialog()
        self.update_sensitivity()

    def add_image(self, url: str, caption: str, fullsize_url: str):
        GLib.idle_add(self._add_image, url, caption, fullsize_url)

    def _add_image(self, url, caption, fullsize_url):
        img = ResizableImage(url, caption)
        def on_image_switched(img):
            self.emit('image-switched')
        img.connect('image-loaded', on_image_switched)
        btn = None
        if self.create_fullsize_dialog and fullsize_url != None:
            btn = Gtk.Button.new()
            btn.set_can_focus(False)
            btn.set_halign(Gtk.Align.FILL); btn.set_valign(Gtk.Align.FILL)
            btn.set_relief(Gtk.ReliefStyle.NONE)
            btn.add(img)
            btn.connect('clicked', self._show_img_dialog)
            self.dialog.gallery.add_image(fullsize_url, caption, None)
            widget = btn
        else:
            widget = img

        widget.show_all()

        i = str(len(self.stack.get_children()))
        if caption != None:
            self.stack.add_titled(widget, i, caption)
        else:
            self.stack.add_named(widget, i)

        self.images.append((btn, img))

        self.update_sensitivity()

    def _show_img_dialog(self, *args):
        self.dialog.set_transient_for(self.get_toplevel())
        self.dialog.show_image(self.stack.get_visible_child_name())
        self.dialog.run()
        self.dialog.hide()

    def remove_all(self):
        for img in self.images:
            img[1].destroy()
            if img[0] != None:
                img[0].destroy()
            del img
        if self.create_fullsize_dialog:
            self.dialog.gallery.remove_all()

    def update_sensitivity(self):
        if self.stack.get_visible_child_name() == None:
            self.btn_prev.set_sensitive(False)
            self.btn_next.set_sensitive(False)
            return
        i = int(self.stack.get_visible_child_name())
        count = len(self.stack.get_children())
        self.btn_prev.set_sensitive(i > 0)
        self.btn_next.set_sensitive(i < count - 1)

    def get_selected_image(self):
        if self.stack.get_visible_child_name() == None:
            return None
        i = int(self.stack.get_visible_child_name())
        return self.images[i][1]

    @Gtk.Template.Callback()
    def on_stack_visible_child_notify(self, obj, prop):
        self.update_sensitivity()
        self.emit('image-switched')

    @Gtk.Template.Callback()
    def on_btn_prev_clicked(self, btn):
        i = int(self.stack.get_visible_child_name())
        if i > 0:
            self.stack.set_visible_child_full(str(i - 1), Gtk.StackTransitionType.OVER_RIGHT)

    @Gtk.Template.Callback()
    def on_btn_next_clicked(self, btn):
        i = int(self.stack.get_visible_child_name())
        if i < len(self.stack.get_children()) - 1:
            self.stack.set_visible_child_full(str(i + 1), Gtk.StackTransitionType.OVER_LEFT)

    @Gtk.Template.Callback()
    def on_key_press_event(self, widget, event: Gdk.EventKey):
        if event.keyval == Gdk.KEY_Left:
            self.btn_prev.clicked()
        if event.keyval == Gdk.KEY_Right:
            self.btn_next.clicked()

class ResizableImage(Gtk.DrawingArea):
    __gsignals__ = {
        'image-loaded': (GObject.SIGNAL_RUN_FIRST, None, ())
    }

    def __init__(self, url, caption):
        super().__init__()
        self.url = url
        self.caption = caption
        self.pixbuf_orig = None
        self.pixbuf = None
        self.cancellable = None
        self.error = None

    def load_image(self):
        self.cancellable = Gio.Cancellable.new()
        def ready(obj, res, stream, pixbuf_file):
            try:
                pixbuf = GdkPixbuf.Pixbuf.new_from_stream_finish(res)
            except GLib.Error as e:
                self.error = _('Cannot load image "{}". Error: {}').format(pixbuf_file.get_uri(), e.message)
                print(self.error)
                return
            self.pixbuf_orig = pixbuf
            self.pixbuf = pixbuf
            self.queue_draw()
            stream.close()
            self.emit('image-loaded')
        def cancel(*args):
            self.cancellable.cancel()
        try:
            pixbuf_file = Gio.File.new_for_uri(self.url)
            stream = pixbuf_file.read()
            GdkPixbuf.Pixbuf.new_from_stream_async(stream, self.cancellable, ready, stream, pixbuf_file)
            self.connect('destroy', cancel)
        except GLib.Error as e:
            self.error = _('Cannot load image "{}". Error: {}').format(pixbuf_file.get_uri(), e.message)
            self.queue_draw()
            print(self.error)
            return

    def do_draw(self, cr):
        allocation = self.get_allocation()
        width = allocation.width
        height = allocation.height

        Gtk.render_background(self.get_style_context(), cr, 0, 0, width, height)

        if self.pixbuf_orig == None:
            if self.error == None:
                Gtk.render_activity(self.get_style_context(),
                    cr, width / 2 - 10, height / 2 - 10, 20, 20)
            else:
                cr.move_to(5, 20)
                cr.show_text(self.error)
            if self.cancellable == None:
                self.load_image()
            return False

        if ((self.pixbuf.get_width() != width and self.pixbuf.get_height() != height)
            or self.pixbuf.get_width() > width or self.pixbuf.get_height() >  height):
            scale = min(width / self.pixbuf_orig.get_width(), height / self.pixbuf_orig.get_height(), 1.0)
            del self.pixbuf
            self.pixbuf = self.pixbuf_orig.scale_simple(
                self.pixbuf_orig.get_width() * scale,
                self.pixbuf_orig.get_height() * scale,
                GdkPixbuf.InterpType.BILINEAR
            )
        Gdk.cairo_set_source_pixbuf(
            cr, self.pixbuf,
            width / 2 - self.pixbuf.get_width() / 2,
            height / 2 - self.pixbuf.get_height() / 2
        )
        cr.paint()

        return False

@Gtk.Template(resource_path='/io/gitlab/FlatStore/appdetailsgallerydialog.ui')
class FlatstoreDetailsGalleryDialog(Gtk.Dialog):
    __gtype_name__ = 'FlatstoreDetailsGalleryDialog'

    box = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(kwargs)
        self.gallery = FlatstoreDetailsGallery(False)
        self.gallery.connect('image-switched', self.update_ui)
        self.box.pack_start(self.gallery, True, True, 0)

    def show_image(self, name):
        self.gallery.stack.set_visible_child_name(name)
        self.update_ui()

    def update_ui(self, *args):
        image = self.gallery.get_selected_image()
        if image == None :
            return

        self.set_title(image.caption)

        parent = self.get_transient_for()
        pixbuf = image.pixbuf_orig
        if pixbuf == None or parent == None:
            return
        width, height = parent.get_size()
        target_w = pixbuf.get_width() + 10
        target_h = pixbuf.get_height() + 10
        scale = min(width / target_w, height / target_h, 1.0)
        self.resize(target_w * scale, target_h * scale)
        
