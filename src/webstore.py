# webstore.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Flatpak', '1.0')

from gi.repository import GLib, GObject, Gtk, Gio
from gi.repository import GdkPixbuf
from gi.repository import Flatpak

from datetime import datetime
from threading import Thread
from xml.etree import ElementTree

from .flatpakutils import *
from .appdetails import FlatstoreAppDetails, language, nsmap

categories = ['Audio', 'AudioVideo', 'Development', 'Education', 'Game',
    'Graphics', 'Network', 'Office', 'Science', 'Utility', 'Video', 'Other']
categories_translated = [_('Audio'), _('Audio & Video'), _('Development'), _('Education'), _('Game'),
    _('Graphics'), _('Network'), _('Office'), _('Science'), _('Utility'), _('Video'), _('Other')]
CATEGORY_OTHER = categories.index('Other')

class FlatStoreWebStoreAppData(GObject.Object):
    ref = None
    categories = None
    keywords = None
    def __init__(self, ref, categories, keywords):
        super().__init__()
        self.ref = ref
        self.categories = categories
        self.keywords = keywords

@Gtk.Template(resource_path='/io/gitlab/FlatStore/webstore.ui')
class FlatstoreWebStore(Gtk.Stack):
    __gtype_name__ = 'FlatstoreWebStore'

    leaflet = Gtk.Template.Child()

    apps_box: Gtk.IconView = Gtk.Template.Child()
    apps_box_r_name: Gtk.CellRendererText = Gtk.Template.Child()
    search_entry = Gtk.Template.Child()
    liststore = Gtk.ListStore(str, GdkPixbuf.Pixbuf, GObject.GType(FlatStoreWebStoreAppData))
    search_filter = liststore.filter_new(None)
    categories_box: Gtk.ListBox = Gtk.Template.Child()
    scrolled_window = Gtk.Template.Child()
    catalog_loading_box = Gtk.Template.Child()
    catalog_loading_progress = Gtk.Template.Child()

    appdetails_column = Gtk.Template.Child()
    details_sw = Gtk.Template.Child()

    can_go_back = GObject.Property(type=bool, default=False)
    loading_appstream = GObject.Property(type=bool, default=False)

    __gsignals__ = {
        'install-app': (GObject.SIGNAL_RUN_FIRST, None,
                      (Flatpak.RemoteRef, str, GdkPixbuf.Pixbuf,)),
        'install-app-bundle': (GObject.SIGNAL_RUN_FIRST, None,
                      (Flatpak.BundleRef,)),
        'remove-app': (GObject.SIGNAL_RUN_FIRST, None,
                      (Flatpak.RemoteRef,))
    }

    appstreams = {}
    selected_app = None

    pixbuf64_package_generic = new_pixbuf_from_iconname('package-x-generic', 64)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.appdetails = None
        for i in range(len(categories)):
            row = Gtk.ListBoxRow.new()
            row.set_size_request(-1, 50)
            row.set_name(categories[i])
            label = Gtk.Label.new(categories_translated[i])
            row.add(label)
            self.categories_box.add(row)
        self.open_category('Audio')
        self.search_filter.set_visible_func(self.search_entry_visible_func)
        self.show_all()

    def refresh(self):
        cancellable = Gio.Cancellable.new()
        def cancellable_cancel(*args):
            cancellable.cancel()
        self.connect("destroy", cancellable_cancel)

        self.catalog_loading_box.show()
        self.scrolled_window.hide()
        self.loading_appstream = True
        self.appstreams.clear()
        import datetime

        xml_lang_attrib = '{http://www.w3.org/XML/1998/namespace}lang'

        def target():
            installation = get_flatpak_installations()[0]
            apps = []
            apps_str = []
            remote_components = {}
            for r in installation.list_remotes():
                if r.get_noenumerate():
                    continue
                self.refresh_set_progress(_('Updating catalog "{}"...').format(r.get_name()))
                appstream_dir = r.get_appstream_dir(None)
                appstream_icons = appstream_dir.get_child('icons').get_child('64x64');
                appstream_icons_128 = appstream_dir.get_child('icons').get_child('128x128');
                appstream = get_appstream_for_remote_dir(installation, r)
                if not appstream_icons.query_exists():
                    appstream_icons = None

                def add_appstream(remote, appstream, icons_dir):
                    self.appstreams[remote] = (appstream, icons_dir)
                    self.loading_appstream = False
                GLib.idle_add(add_appstream, r.get_name(), appstream, appstream_icons_128)
                refs = installation.list_remote_refs_sync_full(r.get_name(), Flatpak.QueryFlags.NONE)
                refs_len = len(refs)
                for i in range(refs_len):
                    if i % 10 == 0:
                        if cancellable.is_cancelled():
                            return
                        self.refresh_set_progress(
                            _('Loading apps from "{}"...').format(r.get_name()) +
                            '({0:.0%})'.format(i / refs_len))
                    ref = refs[i]
                    if ref.get_kind() != Flatpak.RefKind.APP:
                        continue
                    if ref.get_arch() != Flatpak.get_default_arch():
                        continue
                    if ref.get_name() in apps_str:
                        continue
                    apps_str.append(ref.get_name())
                    if appstream != None:
                        component = get_app_appstream(appstream, ref.get_name())
                    else:
                        component = None
                    remote_components[ref.get_name()] = (component, appstream_icons)
                    # Get name
                    name = None
                    if component != None:
                        name = get_translated_element_text(component, 'name')
                    if name == None:
                        name = ref.get_name()

                    app_categories = []
                    if component != None:
                        c = component.find('categories')
                        if c != None:
                            for text in c.findall('category'):
                                if text.text in categories:
                                    app_categories.append(categories.index(text.text))
                    if len(app_categories) == 0:
                        app_categories.append(CATEGORY_OTHER)

                    keywords = []
                    if component != None:
                        c = component.find('keywords')
                        if c != None:
                            for keyword in c.findall('keyword'):
                                if ((not xml_lang_attrib in keyword.attrib) or
                                    (xml_lang_attrib in keyword.attrib and keyword.attrib[xml_lang_attrib] == language)):
                                    keywords.append(keyword.text.lower())

                    info = FlatStoreWebStoreAppData(ref, app_categories, keywords)
                    apps.append((name, info))

            apps.sort(key=lambda app: app[0].lower())
            def add_apps(apps):
                apps_iters = {}
                for app in apps:
                    i = self.liststore.append([app[0], self.pixbuf64_package_generic, app[1]])
                    apps_iters[app[1].ref.get_name()] = i
                self.catalog_loading_box.hide()
                self.scrolled_window.show()
                def set_pixbuf(app_name, pixbuf):
                    self.liststore.set_value(apps_iters[app_name], 1, pixbuf)
                    return False
                def load_pixbufs():
                    # Load pixbufs
                    for name in remote_components:
                        if cancellable.is_cancelled():
                            return
                        item = remote_components[name]
                        component = item[0]
                        icons_dir= item[1]
                        pixbuf = get_icon_component(component, icons_dir)
                        if pixbuf == None: continue
                        GLib.idle_add(set_pixbuf, name, pixbuf)
                thread = Thread(target=load_pixbufs)
                thread.start()
            GLib.idle_add(add_apps, apps)

        thread = Thread(target=target)
        thread.start()

    def refresh_set_progress(self, text):
        def idle_fun(text):
            self.catalog_loading_progress.set_label(text)
        GLib.idle_add(idle_fun, text)

    def go_back(self):
        if self.get_visible_child_name() != 'details':
            self.leaflet.set_visible_child_name('categories_list')
        self.set_visible_child_name('apps')
        self.search_entry.set_text("")

    @Gtk.Template.Callback()
    def update_can_go_back(self, *args):
        if ((not self.leaflet.props.folded) and
            self.get_visible_child_name() == 'apps' and
            self.categories_box.get_visible()):
            self.can_go_back = False
            return
        self.can_go_back = (
            self.leaflet.get_visible_child_name() == 'app_list' or
            self.get_visible_child_name() == 'details')

    @Gtk.Template.Callback()
    def on_categories_box_row_activated(self, list_box, row):
        self.search_entry.set_text("")
        self.open_category(row.get_name())
        self.leaflet.set_visible_child_name('app_list')

    @Gtk.Template.Callback()
    def apps_box_set_narrow(self, *args):
        width = self.apps_box.get_allocation().width
        is_narrow = width < 600
        self.apps_box_r_name.props.width_chars = 11 if is_narrow else 20

        #FIXME: Use more proper way of forcing icon view to redraw
        allocation = self.apps_box.get_allocation()
        self.apps_box.do_size_allocate(self.apps_box, allocation)

    @Gtk.Template.Callback()
    def on_apps_box_item_activated(self, icon_view, path):
        app = icon_view.get_model()[path][2].ref
        self.show_app(app)

    def open_category(self, category):
        if category == None or (not category in categories):
            self.apps_box.set_model(search_filter)
            self.categories_box.unselect_all()
            return
        category = categories.index(category)
        category_filter = self.liststore.filter_new(None)
        category_filter.set_visible_func(self.search_entry_visible_func_category, category)
        category_filter.refilter()
        self.apps_box.set_model(category_filter)
        self.categories_box.select_row(self.categories_box.get_row_at_index(category))

    def show_app(self, app, installation = None):
        self.selected_app = app

        if self.appdetails != None:
            self.appdetails.destroy()

        self.appdetails = FlatstoreAppDetails()
        self.appdetails.connect('install-app', self.on_details_install_clicked)
        self.appdetails.connect('remove-app', self.on_details_remove_clicked)
        self.appdetails_column.add(self.appdetails)

        if self.appdetails.open_app(app, self.appstreams, installation):
            self.set_visible_child_name('details')
            self.details_sw.set_placement(Gtk.CornerType.TOP_LEFT)
            self.update_can_go_back()

    def on_details_install_clicked(self, widget, name, pixbuf):
        print(widget)
        if self.selected_app != None:
            if type(self.selected_app) is Flatpak.BundleRef:
                self.emit('install-app-bundle', self.selected_app)
            else:
                self.emit(
                    'install-app', self.selected_app, name, pixbuf)

    def on_details_remove_clicked(self, widget, name, pixbuf):
        if self.selected_app != None:
            self.emit('remove-app', self.selected_app)

    @Gtk.Template.Callback()
    def on_search_entry_search_changed(self, entry):
        if entry.get_text() == "":
            self.open_category('Audio')
            self.leaflet.set_visible_child_name('categories_list')
            self.categories_box.show()
        else:
            self.apps_box.set_model(self.search_filter)
            self.search_filter.refilter()
            self.categories_box.hide()
            self.leaflet.set_visible_child_name('app_list')
        self.update_can_go_back()

    def search_entry_visible_func(self, model, iter, data):
        # FIXME: Gtk critical error when searching
        search = self.search_entry.get_text().lower()
        target = model[iter][0].lower()
        if search in target: return True
        for keyword in  model[iter][2].keywords:
            if search in keyword:
                return True
        return False

    def search_entry_visible_func_category(self, model, iter, data):
        target = model[iter][2].categories
        return (data in target)
