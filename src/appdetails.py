# appdetails.py
#
# Copyright 2020 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib, GObject, Gtk, Gio
from gi.repository import GdkPixbuf

import locale
from datetime import datetime
from threading import Thread
from xml.etree import ElementTree

from .flatpakutils import *
from .permissionsdialog import FlatstorePermissionsDialog
from .appdetailsgallery import FlatstoreDetailsGallery

url_names = {
    'homepage': _('Home page'),
    'help': _('Help'),
    'bugtracker': _('Report bugs'),
    'donation': _('Donate'),
    'faq': _('FAQ')
}

@Gtk.Template(resource_path='/io/gitlab/FlatStore/appdetails.ui')
class FlatstoreAppDetails(Gtk.Box):
    __gtype_name__ = 'FlatstoreAppDetails'

    details_name = Gtk.Template.Child()
    details_image = Gtk.Template.Child()
    details_summary = Gtk.Template.Child()
    details_install = Gtk.Template.Child()
    details_remove = Gtk.Template.Child()
    details_description = Gtk.Template.Child()
    details_link_box = Gtk.Template.Child()
    details_developer = Gtk.Template.Child()
    details_license = Gtk.Template.Child()
    details_release = Gtk.Template.Child()
    details_release_date = Gtk.Template.Child()
    details_release_news = Gtk.Template.Child()
    details_origin = Gtk.Template.Child()
    details_size_dl = Gtk.Template.Child()
    details_size = Gtk.Template.Child()
    permissions_btn = Gtk.Template.Child()

    gallery: FlatstoreDetailsGallery = None

    __gsignals__ = {
        'install-app': (GObject.SIGNAL_RUN_FIRST, None,
                      (str, GdkPixbuf.Pixbuf)),
        'remove-app': (GObject.SIGNAL_RUN_FIRST, None,
                      (str, GdkPixbuf.Pixbuf)),
        'open-app': (GObject.SIGNAL_RUN_FIRST, None,
                      ())
    }

    selected_app = FlatstoreDetailsGallery()


    def __init__(self, **kwargs):
        super().__init__(kwargs)

        self.gallery = FlatstoreDetailsGallery()
        self.pack_start(self.gallery, True, True, 0)
        self.gallery.show_all()
        self.gallery.set_size_request(-1, 300)
        self.reorder_child(self.gallery, 1)

    def open_app(self, app, appstreams, installation) -> bool:
        self.emit('open-app')
        self.selected_app = app

        appstream = None
        component = None
        if installation == None:
            installation = get_flatpak_installations()[0]
        try:
            if type(app) is Flatpak.InstalledRef:
                appstream = appstreams[app.get_origin()]
                app_remote = app.get_origin()
            elif type(app) is Flatpak.BundleRef:
                component = get_bundle_appstream(app)
                app_remote = app.get_origin()
                if app_remote == None:
                    app_remote = _('Local file')
                else:
                    app_remote = '<a href="{0}">{0}</a>'.format(app_remote)
            else:
                if app.get_remote_name() in appstreams:
                    appstream = appstreams[app.get_remote_name()]
                app_remote = app.get_remote_name()
        except GLib.Error as e:
            print("appdetails: Cannot open local file: " + e.message)
            return False

        self.details_origin.set_markup(_('<b>Origin:</b> {}').format(app_remote))

        if component == None and type(app) is not Flatpak.BundleRef and appstream != None and appstream[0] != None:
            component = get_app_appstream(appstream[0], app.get_name())

        self.details_remove.hide()
        self.details_install.show()
        for ref in installation.list_installed_refs():
            if ref.get_name() == app.get_name():
                self.details_remove.show()
                if not (type(app) is Flatpak.BundleRef):
                    self.details_install.hide()
                break

        icon = None
        if type(app) is Flatpak.BundleRef:
            icon_bytes = app.get_icon(128)
            if icon_bytes != None:
                loader = GdkPixbuf.PixbufLoader.new_with_type('png')
                loader.write_bytes(icon_bytes)
                loader.close()
                icon = loader.get_pixbuf()
        elif appstream != None:
            icons_dir = appstream[1]
            icon = get_icon_component(component, icons_dir, size = 128)
        if icon != None:
            self.details_image.set_from_pixbuf(icon)
        else:
            self.details_image.set_from_icon_name('package-x-generic', 128)

        self.set_label_component(self.details_name, component, 'name', app.get_name())
        self.set_label_component(self.details_summary, component, 'summary')
        self.set_label_component(self.details_developer, component, 'developer_name', pattern=_('<b>Developer:</b> {}'))
        self.set_label_component(self.details_license, component, 'project_license')

        self.details_size.hide();self.details_size_dl.hide()
        if type(app) == Flatpak.RemoteRef:
            self.details_size.set_markup(_('<b>Installed size: </b> {}').format(get_display_file_size(app.get_installed_size())))
            self.details_size_dl.set_markup(_('<b>Download size: </b> {}').format(get_display_file_size(app.get_download_size())))
            self.details_size.show();self.details_size_dl.show()

        # License link
        if 'LicenseRef-proprietary' in self.details_license.get_label():
            markup = self.details_license.get_label()
            uri_start = markup.find('LicenseRef-proprietary')
            uri_start_url = markup.find('http', uri_start)
            uri_end = markup.find(' ', uri_start)
            if uri_end < 0: uri_end = len(markup)
            if uri_start_url >= 0:
                uri = markup[uri_start_url : uri_end]
            else:
                uri = 'https://en.wikipedia.org/wiki/Proprietary_software'
            new_markup = markup[:uri_start] + '<a href="{}">{}</a>'.format(uri, _('Proprietary')) + markup[uri_end:]
            self.details_license.set_markup(new_markup)
        self.details_license.set_markup(
            _('<b>License:</b> {}').format(self.details_license.get_label()))

        # Description
        description = ""
        if component != None:
            description_element = get_translated_element(component, 'description')
            if description_element != None:
                #print(ElementTree.tostring(component).decode())
                description = self.xml_to_markup(description_element)
        self.details_description.set_markup(description)

        # Version
        release = None
        release_date = None
        release_news = None
        try:
            releases_component = component.find('releases')
            release_component = releases_component.find('release')
            release = release_component.get('version')
            release_date_stamp = release_component.get('timestamp')
            release_data_time = datetime.fromtimestamp(int(release_date_stamp))
            release_date = release_data_time.strftime('%x')
            release_news = self.xml_to_markup(release_component.find('description'))
        except ValueError as e:
            pass
        except AttributeError as e:
            print('appdetails: Release component error')
            pass
        if release != None:
            self.details_release.set_markup(_('<b>Version:</b> {}').format(release))
        self.details_release.set_visible(release != None)
        if release_date != None:
            self.details_release_date.set_markup(_('<b>Updated:</b> {}').format(release_date))
        self.details_release_date.set_visible(release_date != None)
        if release_news != None:
            self.details_release_news.set_markup(_('<b>News:</b>\n{}').format(release_news))
        self.details_release_news.set_visible(release_news != None)

        # Links
        for child in self.details_link_box.get_children():
            child.destroy()
            del child
        if component != None:
            for link in component.findall('url'):
                btn = Gtk.LinkButton()
                btn.set_uri(link.text)
                t = link.get('type', '')
                btn.set_label(url_names.get(t, t))
                self.details_link_box.pack_start(btn, False, False, 0)
                btn.show()

        # Screenshots
        self.gallery.remove_all()

        if component != None:
            screenshots = component.find('screenshots')
            # Prevents showing screenshots in wrong apps
            cancellable = Gio.Cancellable.new()
            def cancel(self, cancellable):
                cancellable.cancel()
            self.connect('open-app', cancel, cancellable)
            def target():
                if screenshots == None: return;
                for s in screenshots.findall('screenshot'):
                    urls = s.findall('image')
                    size = 99999
                    url = None
                    size_max = 0
                    url_max = None
                    for u in urls:
                        height = int(u.get('height', '0'))
                        if height >= 300 and height < size:
                            url = u.text
                            size = height
                        if height > size_max:
                            url_max = u.text
                            size_max = height
                    if url == None: url = s.findtext('image')
                    if url != None:
                        caption = s.findtext('caption')
                        if caption == None:
                            source_img = s.findtext('image[@type="source"]')
                            if source_img != None:
                                caption = os.path.basename(source_img)
                        self.gallery.add_image(url, caption, url_max)
                    if cancellable.is_cancelled():
                        return
            thread = Thread(target=target)
            thread.start()

        if component == None:
            self.details_name.show()
            self.details_name.set_label(app.get_name())
            self.details_description.show()
            self.details_description.set_label(_('No details available'))

        self.permissions_btn.set_visible(app.get_kind() == Flatpak.RefKind.APP)

        return True

    def set_label_component(self, label, component, key, default_val = '', pattern = '{}'):
        if component == None:
            label.hide()
            return

        text = get_translated_element_text(component, key, default_val)

        if text != None and text != '':
            label.show()
            label.set_markup(pattern.format(text.replace("&", "&amp;")))
        else:
            label.hide()

    def xml_to_markup(self, xml_element):
        if xml_element == None:
            return None
        description = ""
        def xml_replace(text, a, b, param):
            text = text.replace('<{}>'.format(a), '<{} {}>'.format(b, param))
            text = text.replace('</{}>'.format(a), '</{}>'.format(b))
            return text
        def to_markup(element, tag):
            text = ElementTree.tostring(element, encoding="utf-8").decode()
            text = text.replace('<{}>'.format(tag), '').replace('</{}>'.format(tag), '')
            text = xml_replace(text, 'em', 'span', 'style="italic"')
            text = xml_replace(text, 'code', 'span', 'font_family="monospace"')
            return text
        for p in xml_element.getchildren():
            if p.tag == "p":
                description += to_markup(p, p.tag) + "\n\n"
            elif p.tag == "ul" or p.tag == "ol":
                l = [to_markup(ch, 'li') for ch in p.getchildren()]
                if p.tag == "ol": l.sort()
                description += "•" + "\n•".join(l) + "\n\n"
            elif p.text != None:
                description += to_markup(p, p.tag) + "\n"

        if description.endswith('\n'):
            description = description[:-1]

        return description

    @Gtk.Template.Callback()
    def on_install_clicked(self, btn):
        dialog = FlatstorePermissionsDialog(self.selected_app, show_buttons=True)
        dialog.set_transient_for(self.get_toplevel())
        response = dialog.run()
        dialog.destroy()
        if response != Gtk.ResponseType.ACCEPT: return
        self.emit('install-app', self.details_name.get_label(), self.details_image.get_pixbuf())

    @Gtk.Template.Callback()
    def on_remove_clicked(self, btn):
        self.emit('remove-app', self.details_name.get_label(), self.details_image.get_pixbuf())

    @Gtk.Template.Callback()
    def on_permissions_btn_clicked(self, btn):
        dialog = FlatstorePermissionsDialog(self.selected_app)
        dialog.set_transient_for(self.get_toplevel())
        dialog.run()
        dialog.destroy()
