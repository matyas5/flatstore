# jobwidget.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Vte', '2.91')

from gi.repository import GObject, GLib, Gtk, Gdk, Gio, GdkPixbuf
from gi.repository import Vte, Handy

from .flatpakutils import get_ref_fancy_name, get_ref_icon_pixbuf

import enum

GObject.type_register(Handy.Dialog)

class FlatstoreJobType(enum.Enum):
    INSTALL = 0
    UNINSTALL = 1
    UPDATE = 2
    OTHER = 3

@Gtk.Template(resource_path='/io/gitlab/FlatStore/jobwidget.ui')
class FlatstoreJobWidget(Gtk.Box):
    __gtype_name__ = 'FlatstoreJobWidget'

    terminal_window = Gtk.Template.Child()
    details_dialog = Gtk.Template.Child()
    response_yes_btn = Gtk.Template.Child()
    response_no_btn = Gtk.Template.Child()
    app_progress_bar = Gtk.Template.Child()
    app_name_label = Gtk.Template.Child()
    app_icon_image = Gtk.Template.Child()
    active = False
    ref = None
    show_notification_when_finished = True

    def __init__(self, command, ref, action_title, app_name = None, app_icon = None, job_type = FlatstoreJobType.OTHER, **kwargs):
        super().__init__(**kwargs)
        self.command = command
        self.title = action_title
        self.job_type = job_type
        if app_name != None:
            self.app_name_label.set_label(app_name)
        else:
            self.app_name_label.set_label(get_ref_fancy_name(ref))
        if app_icon != None:
            app_icon = app_icon.scale_simple(48, 48, GdkPixbuf.InterpType.BILINEAR)
            self.app_icon_image.set_from_pixbuf(app_icon)
        else:
            self.app_icon_image.set_from_pixbuf(get_ref_icon_pixbuf(ref))
        self.app_progress_bar.set_text(_('{} (in queue)').format(action_title))
        self.ref = ref
        self.settings = Gio.Settings('io.gitlab.FlatStore')

    def run(self):
        self.active = True
        self.app_progress_bar.set_text(self.title + '...')
        self.terminal = Vte.Terminal.new()
        self.terminal.set_cursor_shape(Vte.CursorShape.IBEAM)
        self.terminal.set_colors(
            Gdk.RGBA(0, 0, 0), Gdk.RGBA(1, 1, 1), None)
        self.terminal_window.add(self.terminal)
        self.terminal.connect('child-exited', self.on_child_exited)
        self.terminal.show()
        # Spawn process
        self.terminal_pty = Vte.Pty.new_sync(Vte.PtyFlags.DEFAULT, None)
        self.terminal.set_pty(self.terminal_pty)
        result = self.terminal.spawn_sync(
            Vte.PtyFlags.DEFAULT, None, self.command, None, GLib.SpawnFlags.DEFAULT, None, None, None)
        self.terminal_child_pid = result.child_pid
        GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, self.read_from_terminal, 10)

    @Gtk.Template.Callback()
    def on_app_button_clicked(self, btn):
        self.details_dialog.set_transient_for(self.get_toplevel())
        self.details_dialog.run()
        self.details_dialog.hide()

    def on_child_exited(self, terminal, status):
        self.active = False
        self.details_dialog.destroy()
        self.destroy()

    def on_destroy(self):
        self.active = False
        self.terminal_window.destroy()
        GLib.spawn_command_line_async(
            'flatpak-spawn --host kill -s TERM ' + self.terminal_child_pid)

    def read_from_terminal(self, *args):
        if not self.active:
            return False
        outs = Gio.MemoryOutputStream.new_resizable()
        self.terminal.write_contents_sync(outs, 0, None)
        outs.close()
        data = outs.steal_as_bytes().get_data().decode('utf-8')
        lines = data.split('\n')
        if self.read_last_line(lines).lower().replace(' ','').endswith('[y/n]:'):
            self.terminal_bool_question()
        elif self.read_progress(lines):
            return self.active
        self.app_progress_bar.set_text(self.title + '...')
        return self.active

    def read_progress(self, lines):
        for l in lines:
            if '%' in l:
                words = l.split(' ')
                cut_end = 2
                for i in range(len(words)):
                    if words[i].endswith('%'):
                        cut_end = i
                        fraction = int(words[i][:-1])
                        self.app_progress_bar.set_fraction(fraction / 100)
                        break
                # Remove the progress bar
                text_end = 1
                if '/' in words[1]:
                    text_end = 2
                words = words[:text_end] + words[cut_end:]
                while '' in words : words.remove('')
                self.app_progress_bar.set_text(' '.join(words))
                return True
        return False

    def read_last_line(self, lines):
        index = len(lines) - 1
        while index >= 0:
            if lines[index] == '':
                index -= 1
                continue
            return lines[index]
        return ''

    def terminal_bool_question(self):
        self.response_yes_btn.show()
        self.response_no_btn.show()

        if self.settings.get_boolean('auto-dialog-confirm'):
            answer = Gtk.ResponseType.YES
        else:
            self.details_dialog.set_transient_for(self.get_toplevel())
            answer = self.details_dialog.run()
        if answer == Gtk.ResponseType.YES:
            answer = 'y\n'
        else:
            answer = 'n\n'
        self.terminal.feed_child_binary(answer.encode('utf-8'))

        self.details_dialog.hide()
        self.response_yes_btn.hide()
        self.response_no_btn.hide()
