# flatpakutils.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, time
import xml.etree.ElementTree

from gi.repository import GLib, Gtk, Gio
from gi.repository import GdkPixbuf
from gi.repository import Flatpak

datadirs = ['/var/lib/flatpak/exports/share/',
    os.path.expanduser('~/.local/share/flatpak/exports/share/')]
theme = Gtk.IconTheme.get_default()
units = ["Bytes", "KiB", "MiB", "GiB", "TiB", "PiB"]

nsmap = {"xml": "http://www.w3.org/XML/1998/namespace"}
try:
    import locale
    language = locale.getlocale()[0]
    language = language.split('_')[0]
except:
    language = None
    pass

if theme != None:
    for path in datadirs:
        if os.path.isdir(path + 'icons'):
            theme.prepend_search_path(path + 'icons')

def new_pixbuf_from_iconname(icon, size, flags = None):
    """Return GtkPixbuf created from the icon loaded from the current icon theme."""
    theme.rescan_if_needed()
    if not flags == None:
        flags = Gtk.IconLookupFlags.FORCE_SIZE | flags
    else:
        flags = Gtk.IconLookupFlags.FORCE_SIZE

    if theme.has_icon(icon):
        img = theme.load_icon(icon, size, flags)
        return img
    else:
        return theme.load_icon("image-missing", size, flags).copy()

def get_display_file_size(bytes_size):
    """Return a fancy human-readable string with size in the highest units."""
    unit = 0
    bytes_size = float(bytes_size)
    while bytes_size >= 1024 and unit < len(units) - 1:
        unit+=1
        bytes_size = bytes_size / 1024
    return str(round(bytes_size, 2)) + " " + units[unit]

def get_ref_fancy_name(ref):
    name = None
    try:
        name = ref.get_appdata_name()
    except :
        pass
    if name == None:
        name = ref.get_name()
    return name

def get_ref_icon_pixbuf(ref):
    for d in datadirs:
        f = os.path.join(d, 'applications', ref.get_name() + ".desktop")
        if os.path.isdir(d) and os.path.isfile(f):
            kf = GLib.KeyFile.new()
            if kf.load_from_file(f, GLib.KeyFileFlags.NONE):
                iconstr = None
                try:
                    iconstr = kf.get_string('Desktop Entry', 'Icon')
                except:
                   pass
                if iconstr != None:
                    icon = new_pixbuf_from_iconname(iconstr, 48)
                    return icon
    return new_pixbuf_from_iconname('package-x-generic', 48)

def get_flatpak_installations():
    installs = Flatpak.get_system_installations()
    # This doesn't work because user data dir is set to the app directory in ~/.var
    # installs.append(Flatpak.Installation.new_user())
    user_dir = '~/.local/share/flatpak'
    user_installation = Flatpak.Installation.new_for_path(Gio.File.new_for_path(os.path.expanduser(user_dir)), True)
    installs.append(user_installation)
    return installs

def make_command(action, installation = None, root = False):
    cmd = ['/usr/bin/flatpak-spawn', '--host']
    if root:
        cmd.append('pkexec')
    cmd.append('flatpak')
    if installation != None:
        if installation.get_id() == 'user':
            cmd.append('--user')
        else:
            cmd.append('--installation=' + installation.get_id())
    cmd.extend(action.split(' '))
    return cmd

def get_appstream_for_remote_dir(installation, remote):
    remote_dir = remote.get_appstream_dir(None)
    appstream_gz = remote_dir.get_child('appstream.xml.gz')
    appstream_xml = remote_dir.get_child('appstream.xml')

    appstream_timestamp = remote.get_appstream_timestamp()
    if appstream_timestamp.query_exists():
        appstream_info = appstream_timestamp.query_info(
            Gio.FILE_ATTRIBUTE_TIME_CHANGED, Gio.FileQueryInfoFlags.NONE)
        if appstream_info.has_attribute(Gio.FILE_ATTRIBUTE_TIME_CHANGED):
            appstream_age = time.time() - appstream_info.get_attribute_uint64(Gio.FILE_ATTRIBUTE_TIME_CHANGED)
        else:
            appstream_age = 0
    else:
        appstream_age = 60 * 60 * 24 * 7 + 1

    if not (appstream_gz.query_exists() or appstream_xml.query_exists()) or appstream_age > 60 * 60 * 24 * 7:
        try:
            installation.update_appstream_sync(remote.get_name(), None, None)
        except GLib.Error as e:
            print('Using external command to update appstream due to API error: ' + e.message)
            try:
                def update_appstream(root):
                    cmd = make_command('update --assumeyes --appstream {}'.format(remote.get_name()), installation, root=root)
                    GLib.spawn_sync(None, cmd, None, GLib.SpawnFlags.DEFAULT)
                update_appstream(False)
                # This requires root on some distros
                if not appstream_timestamp.query_exists():
                    update_appstream(True)
            except GLib.Error as e2:
                print('Failed to update upstream: ' + e2.message)

    if appstream_gz.query_exists():
        source = appstream_gz.load_bytes()[0]

        decompressor = Gio.ZlibDecompressor.new(Gio.ZlibCompressorFormat.GZIP)
        stream_gz = Gio.MemoryInputStream.new_from_bytes(source)
        stream_data = Gio.ConverterInputStream.new(stream_gz, decompressor)

        appstream = bytes()
        count = 1
        while count > 0:
            buf = stream_data.read_bytes(1024*8)
            appstream += buf.get_data()
            count = buf.get_size()
        root = xml.etree.ElementTree.fromstring(appstream.decode('utf-8'))
        return root

    elif appstream_xml.query_exists():
        return xml.etree.ElementTree.fromstring(
            appstream_gz.load_bytes()[0].decode('utf-8'))
    else:
        return None

def get_app_appstream(appstream, name):
    component = appstream.find(
        "./component[id='{}']".format(name))
    if component == None:
        component = appstream.find(
            "./component[id='{}']".format(name + '.desktop'))
    return component

def get_bundle_appstream(app):
    appstream_compressed = app.get_appstream()
    if appstream_compressed == None:
        return None

    decompressor = Gio.ZlibDecompressor.new(Gio.ZlibCompressorFormat.GZIP)
    stream_gz = Gio.MemoryInputStream.new_from_bytes(appstream_compressed)
    stream_data = Gio.ConverterInputStream.new(stream_gz, decompressor)

    appstream = bytes()
    count = 1
    while count > 0:
        data = stream_data.read_bytes(8192)
        count = data.get_size()
        appstream += data.get_data()
    root = xml.etree.ElementTree.fromstring(appstream.decode('utf-8'))
    component = root.find(
        "./component[@type='desktop']")
    return component

def get_icon_component(component, icons_dir, size = 64):
    pixbuf = None
    if component != None:
        try:
            ic = component.find('icon')
            if ic != None:
                t = None
                if 'type' in ic.attrib:
                    t = ic.attrib['type']
                icon = None
                is_remote = t == 'remote'
                if t == 'cached':
                    icon = icons_dir.get_child(ic.text)
                if is_remote:
                    icon = Gio.File.new_for_uri(ic.text)
                if icon != None and icon.query_exists():
                    if is_remote:
                        cancellable = Gio.Cancellable.new()
                        def cancel():
                            cancellable.cancel()
                            return False
                        GLib.timeout_add_seconds(5, cancel)
                    pixbuf = GdkPixbuf.Pixbuf.new_from_stream_at_scale(
                            icon.read(), size, size, True, cancellable if is_remote else None)
        except GLib.Error as e:
            print(e.message)
    return pixbuf

def get_remote_ref_from_installed(installed_ref, installation):
    ref = installation.fetch_remote_ref_sync(
        installed_ref.get_origin(),
        installed_ref.get_kind(),
        installed_ref.get_name(),
        installed_ref.get_arch(),
        installed_ref.get_branch()
    )
    return ref

def get_translated_element(component, key, default=None):
    element = None
    if language != None:
        element = component.find('{}[@xml:lang="{}"]'.format(key, language), namespaces=nsmap)
    if element == None:
        element = component.find(key)
    return element

def get_translated_element_text(component, key, default=None):
    element = None
    if language != None:
        element = component.find('{}[@xml:lang="{}"]'.format(key, language), namespaces=nsmap)
    if element == None:
        element = component.find(key)
    return element.text if element != None else default
