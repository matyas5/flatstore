# installed.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib.request
import gi

gi.require_version('Flatpak', '1.0')

from gi.repository import GObject, GLib, Gtk, Gio, Gdk
from gi.repository import Flatpak

from .installedapp import FlatstoreInstalledAppWidget, FlatstoreInstalledAppWidgetButtonMode
from .flatpakutils import make_command, get_flatpak_installations
from .jobcontainer import FlatstoreJobContainer, FlatstoreJobType

@Gtk.Template(resource_path='/io/gitlab/FlatStore/installed.ui')
class FlatstoreInstalledView(Gtk.ScrolledWindow):
    __gtype_name__ = 'FlatstoreInstalledView'

    installed_apps_box = Gtk.Template.Child()
    main_box = Gtk.Template.Child()

    __gsignals__ = {
        'show-store': (GObject.SIGNAL_RUN_FIRST, None,
                      (Flatpak.InstalledRef, Flatpak.Installation,))
    }

    installs = []

    def __init__(self, job_queue, **kwargs):
        super().__init__(**kwargs)
        self.settings = Gio.Settings('io.gitlab.FlatStore')
        self.settings.connect('changed', self.on_settings_changed)

        self.job_box = FlatstoreJobContainer(job_queue)
        self.main_box.pack_start(self.job_box, False, False, 0)
        self.main_box.reorder_child(self.job_box, 0)
        job_queue.connect('job-finished', self.on_job_queue_job_finished)

        self.installed_apps_monitors = []
        self.installs = get_flatpak_installations()
        for i in self.installs:
            mon = i.create_monitor()
            mon.connect('changed', self.update_installed_apps)
            self.installed_apps_monitors.append(mon)

        self.update_installed_apps()
        Gtk.IconTheme.get_default().connect('changed', self.update_installed_apps)

    def update_installed_apps(self, *args):
        for w in self.installed_apps_box.get_children():
            w.destroy()
            del w

        for i in self.installs:
            self.add_apps_from_installation(i)

    def add_apps_from_installation(self, installation):
        apps = installation.list_installed_refs_by_kind(Flatpak.RefKind.APP)
        if self.settings.get_boolean('runtimes-visible'):
            apps.extend(
                installation.list_installed_refs_by_kind(Flatpak.RefKind.RUNTIME))
        for app in apps:
            if app.get_name().endswith('.Locale'):
                continue
            widget = FlatstoreInstalledAppWidget(
                app, FlatstoreInstalledAppWidgetButtonMode.UNINSTALL, installation)
            widget.connect('remove-app', self.on_remove_app)
            widget.connect('show-store', self.on_show_store)
            self.installed_apps_box.pack_start(widget, True, False, 0)

    def on_remove_app(self, widget, ref):
        self.remove_app(ref, widget.installation)

    def on_show_store(self, widget, ref):
        self.emit('show-store', ref, widget.installation)

    def install_app(self, ref, app_name = None, app_icon = None):
        self.job_box.add_job(
            make_command(
                'install {} {}'.format(ref.get_remote_name(), ref.format_ref()), self.installs[0]),
            ref, _('Installing'), app_name, app_icon, job_type=FlatstoreJobType.INSTALL)

    def install_app_bundle(self, ref):
        self.job_box.add_job(
            make_command(
                'install {}'.format(ref.get_file().get_path()),self.installs[0]),
            ref, _('Installing'), job_type=FlatstoreJobType.INSTALL)

    def remove_app(self, ref, installation=None):
        if installation == None:
            installation = self.installs[0]
        self.job_box.add_job(
            make_command(
                'remove ' + ref.format_ref(), installation),
            ref, _('Uninstalling'), job_type=FlatstoreJobType.UNINSTALL)

    def on_settings_changed(self, settings, key):
        if key == 'runtimes-visible':
            self.update_installed_apps()

    def on_job_queue_job_finished(self, job_queue, job):
        if self.get_toplevel().get_window().get_state() & Gdk.WindowState.FOCUSED:
            return
        if not job.show_notification_when_finished:
            return
        notif = Gio.Notification.new(_('{} {} finished.').format(job.title, job.app_name_label.get_label()))
        notif.set_icon(Gio.ThemedIcon.new('io.gitlab.FlatStore'))
        self.get_toplevel().get_application().send_notification(
            None, notif)
