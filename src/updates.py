# updates.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

from gi.repository import GObject, GLib, Gtk, Gdk, Gio
from gi.repository import Flatpak

from threading import Thread

from .installedapp import FlatstoreInstalledAppWidget, FlatstoreInstalledAppWidgetButtonMode
from .flatpakutils import get_flatpak_installations, make_command
from .jobcontainer import FlatstoreJobContainer, FlatstoreJobType

@Gtk.Template(resource_path='/io/gitlab/FlatStore/updates.ui')
class FlatstoreUpdatesView(Gtk.ScrolledWindow):
    __gtype_name__ = 'FlatstoreUpdatesView'

    main_box = Gtk.Template.Child()
    updates_box = Gtk.Template.Child()
    unused_box = Gtk.Template.Child()
    unsued_label = Gtk.Template.Child()
    refresh_progress = Gtk.Template.Child()
    refresh_spinner = Gtk.Template.Child()
    refresh_button = Gtk.Template.Child()
    update_all_button = Gtk.Template.Child()
    no_updates_label = Gtk.Template.Child()

    check_pending = False
    check_active = False

    __gsignals__ = {
        'show-store': (GObject.SIGNAL_RUN_FIRST, None,
                      (Flatpak.InstalledRef, Flatpak.Installation,))
    }

    show_updates_done = False

    def __init__(self, job_queue, **kwargs):
        super().__init__(**kwargs)
        self.installs = get_flatpak_installations()
        self.check_updates()

        self.job_box = FlatstoreJobContainer(job_queue)
        self.main_box.pack_start(self.job_box, False, False, 0)
        self.main_box.reorder_child(self.job_box, 0)

        job_queue.connect('job-finished', self.on_job_queue_job_finished)

    def on_update_app(self, widget, ref, show_notification_when_finished):
        self.job_box.add_job(
            make_command('update ' + ref.format_ref(), widget.installation), ref, _('Updating'), job_type=FlatstoreJobType.UPDATE,
            show_notification_when_finished=show_notification_when_finished)
        widget.destroy()

    def on_remove_app(self, widget, ref):
        self.job_box.add_job(
            make_command('remove ' + ref.format_ref(), widget.installation), ref, _('Uninstalling'), job_type=FlatstoreJobType.UNINSTALL,
            show_notification_when_finished=True)
        widget.destroy()

    def on_job_queue_job_finished(self, job_queue, job):
        self.check_updates()

    def check_updates(self):
        if self.check_active:
            self.check_pending = True
            return
        self.check_pending = False
        self.check_active = True
        self.set_refresh_widgets(True)
        for w in self.updates_box.get_children():
            w.destroy()
            del w
        for w in self.unused_box.get_children():
            w.destroy()
            del w
        self.no_updates_widgets_set_visible()
        def load_updates():
            updates_available = False
            installsc = len(self.installs)
            for i in range(installsc):
                installation = self.installs[i]
                text = _('Refreshing "{}"...\n({}/{})').format(
                    installation.get_display_name(),
                    str(i + 1),
                    str(installsc))
                GLib.idle_add(
                    self.refresh_progress.set_label, text)
                updates_count = self.check_updates_installation(installation)
                if updates_count > 0:
                    updates_available = True
            GLib.idle_add(self.set_refresh_widgets, False)
            GLib.idle_add(self.no_updates_widgets_set_visible)
            def show_notification():
                if updates_available:
                    if not self.get_toplevel().get_visible():
                        # Show notification if window is not visible
                        notif = Gio.Notification.new(_('New updates are available'))
                        notif.set_default_action('app.show-updates')
                        notif.set_icon(Gio.ThemedIcon.new('io.gitlab.FlatStore'))
                        self.get_toplevel().get_application().send_notification(
                            None, notif)
                elif not (self.get_toplevel().get_window() == None or
                    (self.get_toplevel().get_window().get_state() & Gdk.WindowState.FOCUSED)):
                    if self.show_updates_done:
                        self.show_updates_done = False
                        notif = Gio.Notification.new(_('Updates installed'))
                        notif.set_icon(Gio.ThemedIcon.new('io.gitlab.FlatStore'))
                        self.get_toplevel().get_application().send_notification(
                                None, notif)
                self.check_active = False
                if not self.get_toplevel().get_visible():
                    def timeout():
                        if not self.get_toplevel().get_visible():
                            sys.exit(0)
                    GLib.timeout_add_seconds(15, timeout)
            GLib.idle_add(show_notification)
            if self.check_pending:
                GLib.idle_add(self.check_updates)
        thread = Thread(target=load_updates)
        thread.start()

    def check_updates_installation(self, installation):
        def load_updates_finish(updates, unused):
            already_updating = []
            for w in self.job_box.job_box.get_children():
                already_updating.append(w.ref.get_name())
            for app in updates:
                #if app.get_name().endswith('.Locale'):
                #    continue
                if app.get_name() in already_updating:
                    continue
                widget = FlatstoreInstalledAppWidget(
                    app, FlatstoreInstalledAppWidgetButtonMode.UPDATE, installation)
                widget.connect('update-app', self.on_update_app)
                widget.connect('show-store', self.on_show_store)
                self.updates_box.pack_start(widget, True, False, 0)
            for app in unused:
                if app.get_name() in already_updating:
                    continue
                widget = FlatstoreInstalledAppWidget(
                    app, FlatstoreInstalledAppWidgetButtonMode.UNINSTALL, installation)
                widget.connect('remove-app', self.on_remove_app)
                widget.connect('show-store', self.on_show_store)
                self.unused_box.pack_start(widget, True, False, 0)
        #for remote in installation.list_remotes():
        #    installation.update_remote_sync(remote.get_name())
        unused = installation.list_unused_refs(None)
        updates = installation.list_installed_refs_for_update()
        GLib.idle_add(load_updates_finish, updates, unused)
        return len(updates)

    def set_refresh_widgets(self, refreshing):
        self.refresh_spinner.props.active = refreshing
        self.refresh_spinner.set_visible(refreshing)
        self.refresh_progress.set_visible(refreshing)
        self.refresh_button.set_visible(not refreshing)
        self.update_all_button.set_visible(not refreshing)

    @Gtk.Template.Callback()
    def on_update_all_button_clicked(self, btn):
        for child in self.updates_box.get_children():
            child.update(False)
        self.show_updates_done = True

    @Gtk.Template.Callback()
    def on_refresh_button_clicked(self, btn):
        self.check_updates()

    @Gtk.Template.Callback()
    def no_updates_widgets_set_visible(self, *args):
        child_count = len(self.updates_box.get_children())
        self.no_updates_label.set_visible(
            child_count == 0)
        self.update_all_button.set_visible(
            child_count > 0)

        child_count = len(self.unused_box.get_children())
        self.unsued_label.set_visible(child_count > 0)

        if self.refresh_spinner.get_visible():
            self.no_updates_label.set_visible(False)
            self.unsued_label.set_visible(False)

    def on_show_store(self, widget, ref):
        self.emit('show-store', ref, widget.installation)
