# jobcontainer.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GObject, GdkPixbuf

from .jobwidget import FlatstoreJobWidget, FlatstoreJobType

@Gtk.Template(resource_path='/io/gitlab/FlatStore/jobcontainer.ui')
class FlatstoreJobContainer(Gtk.Box):
    __gtype_name__ = 'FlatstoreJobContainer'

    job_box = Gtk.Template.Child()

    def __init__(self, job_queue, **kwargs):
        super().__init__(**kwargs)
        self.job_queue = job_queue

    def add_job(self, command, ref, title, app_name: str = None,
                app_icon: GdkPixbuf.Pixbuf = None, job_type: FlatstoreJobType = FlatstoreJobType.OTHER,
                show_notification_when_finished = True):
        job = FlatstoreJobWidget(command, ref, title, app_name, app_icon, job_type)
        job.show_notification_when_finished = show_notification_when_finished
        self.job_box.pack_start(job, True, False, 0)
        self.update_job_box()
        self.job_queue.add_job(job)

    @Gtk.Template.Callback()
    def update_job_box(self, *args):
        jobs = self.job_box.get_children()
        if len(jobs) > 0:
            self.show_all()
        else:
            self.set_visible(False)

class FlatstoreJobQueue(GObject.Object):
    jobs = []

    __gsignals__ = {
        'job-finished': (GObject.SIGNAL_RUN_FIRST, None,
                      (FlatstoreJobWidget,)),
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def add_job(self, job):
        self.jobs.append(job)
        job.connect('destroy', self.on_job_finished)
        self.try_start()

    def on_job_finished(self, job):
        if job in self.jobs:
            self.jobs.remove(job)
        self.try_start()
        self.emit('job-finished', job)

    def try_start(self):
        if len(self.jobs) == 0:
            return
        # Start new job if no other is running
        for job in self.jobs:
            if job.active:
                return
        self.jobs[0].run()
