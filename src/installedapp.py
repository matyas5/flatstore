# installedapp.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import GObject, GLib, Gio, Gtk
from gi.repository import Flatpak

from flatstore import flatpakutils

import os, enum, shutil
from threading import Thread

class FlatstoreInstalledAppWidgetButtonMode(enum.Enum):
    NONE = 0
    UNINSTALL = 1
    UPDATE = 2

@Gtk.Template(resource_path='/io/gitlab/FlatStore/installedapp.ui')
class FlatstoreInstalledAppWidget(Gtk.Box):
    __gtype_name__ = 'FlatstoreInstalledAppWidget'

    __gsignals__ = {
        'remove-app': (GObject.SIGNAL_RUN_FIRST, None,
                      (Flatpak.InstalledRef,)),
        'update-app': (GObject.SIGNAL_RUN_FIRST, None,
                      (Flatpak.InstalledRef, bool,)),
        'show-store': (GObject.SIGNAL_RUN_FIRST, None,
                      (Flatpak.InstalledRef,))
    }

    app_name_label = Gtk.Template.Child()
    app_icon_image = Gtk.Template.Child()
    app_decsription_label = Gtk.Template.Child()
    remove_button = Gtk.Template.Child()
    update_button = Gtk.Template.Child()
    actions_popover = Gtk.Template.Child()
    info_button = Gtk.Template.Child()
    show_store_btn = Gtk.Template.Child()
    actions_box = Gtk.Template.Child()
    clean_cache_btn = Gtk.Template.Child()
    clean_config_btn = Gtk.Template.Child()
    clean_data_btn = Gtk.Template.Child()

    def __init__(self, ref, mode, installation, **kwargs):
        super().__init__(**kwargs)
        self.installation = installation
        self.app_name_label.set_label(flatpakutils.get_ref_fancy_name(ref))
        self.app_icon_image.set_from_pixbuf(flatpakutils.get_ref_icon_pixbuf(ref))
        self.ref = ref
        self.mode = mode
        self.update_description()

        if self.mode == FlatstoreInstalledAppWidgetButtonMode.UNINSTALL:
            self.remove_button.show()
        elif self.mode == FlatstoreInstalledAppWidgetButtonMode.UPDATE:
            self.update_button.show()

        # Clean buttons
        self.clean_cache_btn.connect('clicked', self.on_clean_btn_clicked, 'cache')
        self.clean_config_btn.connect('clicked', self.on_clean_btn_clicked, 'config')
        self.clean_data_btn.connect('clicked', self.on_clean_btn_clicked, 'data')

        self.show_all()

    @Gtk.Template.Callback()
    def on_remove_button_clicked(self, btn):
        self.emit('remove-app', self.ref)

    @Gtk.Template.Callback()
    def on_update_button_clicked(self, btn):
        self.update(True)

    def update(self, show_notification_when_finished):
        self.emit('update-app', self.ref, show_notification_when_finished)

    def update_description(self):
        items = []
        items.append(self.ref.get_branch())
        if isinstance(self.ref, Flatpak.InstalledRef) and self.mode != FlatstoreInstalledAppWidgetButtonMode.UPDATE:
            items.append(flatpakutils.get_display_file_size(self.ref.get_installed_size()))
        if self.installation.get_is_user():
            items.append(self.installation.get_display_name())
        self.app_decsription_label.set_label('•'.join(items))

    @Gtk.Template.Callback()
    def on_info_button_clicked(self, btn):
        self.show_store_btn.set_sensitive(False)
        for r in self.installation.list_remotes():
            if r.get_noenumerate():
                continue
            if r.get_name() == self.ref.get_origin():
                self.show_store_btn.set_sensitive(True)
                break

        cancellable = Gio.Cancellable()
        self.update_clean_button(self.clean_cache_btn, 'cache', _('Clean cache'), cancellable)
        self.update_clean_button(self.clean_config_btn, 'config', _('Clean config'), cancellable)
        self.update_clean_button(self.clean_data_btn, 'data', _('Clean data'), cancellable)
        def cancel(popover):
            cancellable.cancel()
        self.actions_popover.connect('closed', cancel)

        self.actions_popover.popup()

    @Gtk.Template.Callback()
    def on_show_store_btn_clicked(self, btn):
        self.actions_popover.hide()
        self.emit('show-store', self.ref)

    def update_clean_button(self, btn, directory, label, cancellable):
        directory = os.path.join(
            os.path.expanduser('~/.var/app'), self.ref.get_name(), directory)
        btn.set_sensitive(os.path.isdir(directory))

        if os.path.isdir(directory):
            btn.set_label('{} (...)'.format(label))

            def get_dir_size(directory, btn, label):
                total_size = 0
                for dirpath, dirnames, filenames in os.walk(directory):
                    for f in filenames:
                        if cancellable.is_cancelled():
                            return
                        fp = os.path.join(dirpath, f)
                        if not os.path.islink(fp):
                            total_size += os.path.getsize(fp)
                def set_label(btn, text):
                    btn.set_label(text)
                text = '{} ({})'.format(label, flatpakutils.get_display_file_size(total_size))
                GLib.idle_add(set_label, btn, text)
            thread = Thread(target=get_dir_size, args=(directory,btn,label,))
            thread.start()
        else:
            btn.set_label(label)

    def on_clean_btn_clicked(self, btn, directory):
        self.actions_popover.popdown()
        if directory == "data":
            dialog = Gtk.MessageDialog(self.get_toplevel(), 0, Gtk.MessageType.QUESTION,
                Gtk.ButtonsType.YES_NO, _("Do you really wish to delete data of the application?"))
            dialog.props.secondary_text = _("You may loose your configuration of the app.")
            result = dialog.run()
            dialog.destroy()
            if result != Gtk.ResponseType.YES:
                return

        def target(directory):
            directory = os.path.join(
                os.path.expanduser('~/.var/app'), self.ref.get_name(), directory)
            shutil.rmtree(directory, ignore_errors=True)
        thread = Thread(target=target, args=(directory,))
        thread.start()
