# main.py
#
# Copyright 2019 matyas5
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio, GLib

from .window import FlatstoreWindow


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='io.gitlab.FlatStore',
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE)
        self.add_main_option("auto-update", ord("a"), GLib.OptionFlags.NONE,
                             GLib.OptionArg.NONE, _("Check for updates and notify if they are available."), None)

    def do_startup(self):
        Gtk.Application.do_startup(self)
        # Actions
        updates_action = Gio.SimpleAction.new('show-updates', None)
        updates_action.connect('activate', self.on_show_updates)
        self.add_action(updates_action)

    def do_activate(self):
        self.make_window().present()

    def make_window(self):
        win = self.props.active_window
        if not win:
            win = FlatstoreWindow(application=self)
        return win

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()
        # convert GVariantDict -> GVariant -> dict
        options = options.end().unpack()

        win = self.make_window()
        do_present = True

        if "auto-update" in options:
               do_present = False

        if do_present:
            win.present()

        if len(command_line.get_arguments()) >= 2 and do_present:
            _file = os.path.abspath(command_line.get_arguments()[1])
            win.open_local_file(_file)

        return 0

    def on_show_updates(self, action, param):
        win = self.props.active_window
        win.view_stack.set_visible_child(win.updates)
        self.activate()

def main(version):
    app = Application()
    return app.run(sys.argv)
